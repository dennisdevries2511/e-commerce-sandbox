import { MutationTree } from 'vuex/types'

export const state = () => ({
  loading: false,
})

export type RootState = ReturnType<typeof state>

export const mutations: MutationTree<RootState> = {
  setLoading(state: RootState, loading: boolean = false) {
    state.loading = loading
  },
}
