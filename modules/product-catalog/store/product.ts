import { MutationTree, ActionTree, GetterTree } from 'vuex/types'

export const state = () => ({})

export type RootState = ReturnType<typeof state>

export const mutations: MutationTree<RootState> = {}

export const actions: ActionTree<RootState, RootState> = {}

export const getters: GetterTree<RootState, RootState> = {}
