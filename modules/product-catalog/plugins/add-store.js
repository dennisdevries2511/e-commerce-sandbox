import * as ProductStore from '~/modules/product-catalog/store/product.ts'

export default ({ store }) => {
  store.registerModule('product', ProductStore)
}
