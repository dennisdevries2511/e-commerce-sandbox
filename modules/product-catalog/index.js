import { resolve } from 'path'

export default function ProductCatalogModule(_moduleOptions) {
  this.extendRoutes((routes) => {
    routes.unshift({
      name: 'product',
      path: '/product/:slug/',
      component: resolve(__dirname, 'pages/Product.vue')
    })
  })

  this.addPlugin(resolve(__dirname, 'plugins/add-store.js'))
}
